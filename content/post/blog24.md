---
title: Verder met onderzoek & concepten!
date: 2017-11-27
---

## Week 3 Maandag - 27/11/2017

* Merkanalyse
* Flowchart
* Wireframes

Het was niet helemaal duidelijk dat er vorige week een validatie was van de merkanalyse dus heb ik daarvan vandaag algemeen feedback gevraagd van Isabella. Na de feedback ben ik de merkanalyse gaan aanpassen.

Ik zou een van de low fid prototypes gaan maken. Daarbij heb ik eerst een flowchart gemaakt. Vanuit daar ben ik wireframes gaan schetsen.
