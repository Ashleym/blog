---
title: Ordenen!  
date: 2017-12-13
---

## Week 5 Woensdag - 13/12/2017

* Nieuw concept
* Schetsen
* Merkanalyse

Vandaag is het nieuwe concept vast gelegd. Daardoor kon ik beginnen met het maken van schetsen/wireframes voor een prototype.
Daarnaast heb ik de merkanalyses afgemaakt voor de volgende validatie -> aankomende vrijdag.
