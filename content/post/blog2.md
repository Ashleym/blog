---
title: Dag 2 - Iteratie 1
date: 2017-09-06
---

## Week 1 Woensdag - 06/09/2017

* Onderzoek
* Onderzoek visueel
* Moodboard doelgroep
* Concept

Vandaag hebben wij vooral aan onze individuele opdrachten gewerkt. Daaronder valt het onderzoek dat visueel gemaakt moet worden en een moodboards over de gekozen doelgroep. Daarnaast zijn wij gaan brainstormen over de game. Ik heb nog wat onderzoek gedaan naar AR (augmented reality) omdat wij dat willen gaan gebruiken in onze game. Er bestaan al wat apps met ar, die ben ik gaan spelen om te kijken hoe dat precies te werk gaat .

Concept #1
In het begin krijgt elk groepje een stapel met kaartjes. De kaartjes hebben twee kanten. Op de ene kant staat een abstracte design van een gebouw (dat niet voor iedereen meteen herkenbaar is) uit Rotterdam dat een QR code is. Op de andere kant (achterkant) staat er in stappen uitgelegd wat je moet doen. Als volgt -> 1. Download de app “ ” -> 2. Scan de QR code, het kaartje van de andere kant -> 3. Volg de instructies op de app. Als eerst krijg je een intro op de app te zien (in een light box) daar word stap voor stap uitgelegd van wat waar staat en wat, wat betekent,  dat doe je door naar links te swipen.  Daarna begint de game. Als je het kaartje scant dan krijg je via je telefoon in AR een gebouw te zien. Dat is het gebouw dat je in Rotterdam moet gaan zoeken. In het menu bij collect kan je zien langs welke gebouwen je al bent geweest (dat word automatisch toegevoegd als je het kaartje hebt gescand).
