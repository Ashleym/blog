---
title: Dag 17 - Iteratie 3
date: 2017-10-23
---

## Week 8 Maandag - 23/10/2017

* Voorbereiding expo
* Filmpje maken
* Werken aan blog
* Werken aan leerdossier

Vandaag zijn er de laatste voorbereidingen gedaan voor de expo. We hebben dingen uitgeprint van wat we willen laten zien. Kleine aanpassingen gedaan aan het filmpje en voor de rest gewerkt aan de blog en leerdossier.
