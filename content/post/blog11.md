---
title: Dag 11 - Iteratie 2
date: 2017-09-27
---

## Week 4 Woensdag - 27/09/2017

* Research Rotterdamse evenementen
* Concept uitgewerkt
* Planning nagelopen

We willen Rotterdamse evenementen toepassen in onze game zodat de studenten Rotterdam beter leert kennen. We hebben daarnaar research gedaan. Vanuit de research hebben we het concept meer uitgewerkt.

Voor de rest hebben we de planning nog wat aangepast.
