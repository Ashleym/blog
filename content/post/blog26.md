---
title: Het is al weer December!
date: 2017-12-4
---

## Week 4 Maandag - 4/12/2017

* Spin wheel
* Low fid prototype
* Low fid prototype validatie
* Concept aangepast

Floris had hulp nodig bij het maken van zijn low fid prototype omdat die van hem best wel wat tijd kostte. Daardoor heb ik een deel van zijn design gemaakt.

Daarnaast heb ik mijn low fid prototype zelf ook afgemaakt en heb ik dat vandaag bij Mio laten valideren. Mijn prototype was goedgekeurd en leerdossier waardig.

Wij zijn met het team het concept gaan bespreken en hebben besloten om het aan te passen. Want uit onderzoek en het testen van de prototype bleek het toch niet helemaal goed te werken.
