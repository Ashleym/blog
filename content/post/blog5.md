---
title: Dag 5 - Iteratie 1
date: 2017-09-12
---

## Week 2 Dinsdag - 12/09/2017

* Design paper prototype
* Paper prototype
* Hoorcollege

Na het hoorcollege zijn wij verder gegaan aan ons project. Wij hebben het design van de paper prototype vastgelegd en zijn dat gaan maken. Daar zijn wij ongeveer in totaal 2/3 uurtjes mee bezig geweest. Mijn taak was het maken van de schetsen van de prototype.

![#Hoorcollege](../img/hc1.JPG)
