---
title: Dag 12 - Iteratie 2
date: 2017-09-28
---

## Week 4 Donderdag - 28/09/2017

* Logo
* Spelregels
* Presentatie maken

Na onze keuzevak zijn Nuray en ik begonnen met het maken van de logo en presentatie. Doordat wij eerder op school waren hebben wij het werk even verdeeld en ben ik de presentatie alvast in elkaar gaan zetten. Nadat we met onze team compleet waren hebben wij weer een taakverdeling gedaan en zijn wij gaan kijken met wat er allemaal nog precies gedaan moest worden. We hebben even met z’n alle het spel goed doorgenomen en hebben de game en spelregels in een document gezet.  
