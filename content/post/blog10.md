---
title: Dag 10 - Iteratie 2
date: 2017-09-25
---

## Week 4 Maandag - 25/09/2017

* Concepten maken
* Concept vastleggen
* Feedback
* Creatieve technieken toepassen

Vorige week hebben we allemaal wat inspiratie opgedaan door o.a wat games te gaan spelen. We hebben onze concepten met elkaar besproken. Daaruit bleek dat de meeste wel iets puzzel achtigs hadden verzonnen. Wij hebben een concept gekozen dat ons allemaal het leukst leek. Het inspiratie van de concept dat ons het beste en leukste leek kwam van mijn telefoon toen ik naar mijn games keek. Daarin zaten o.a de games: Monument Valley, Roll the Ball en Dancing Line. Met ons concept dat hadden vastgelegd zijn wij naar Bob gegaan die ons feedback heeft gegeven. De feedback heeft ons erg goed geholpen en daardoor hebben wij nieuwe ideeën gekregen. Op basis daarvan hebben wij het concept wat aan gepast en ietsjes uitgebreid.
