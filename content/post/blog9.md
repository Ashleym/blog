---
title: Dag 9 - Iteratie 2
date: 2017-09-20
---

## Week 3 Woensdag - 20/09/2017

* Planning en taakverdeling
* Verbeterende mood board
* Concepten maken
* Workshop mindmap

Vandaag zijn wij begonnen met het maken van een nieuwe planning en taakverdeling voor de komende weken. In Excel heb ik weer even een planning gemaakt en in de Google Drive gezet. Daarnaast hebben we individueel een verbeterende mood board gemaakt over ons doel groep cmd en zijn we begonnen met concepten maken. We hebben voorgenomen dat ieder zelf een concept gaat verzinnen voor de game.
