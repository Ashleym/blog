---
title: Last day!!  
date: 2017-12-20
---

## Week 6 Woensdag - 20/12/2017

* HTML/CSS
* Design rationale

Vandaag was het de laatste dag voor de vakantie, iedereen z'n motivatie was een beetje weg waaronder die van mij ook beetje.

Daardoor ben ik Nuray gaan helpen met haar website in html en css. Ze had een workshop daarover gevolgd maar een aantal dingen waren nog niet duidelijk en ze wilde er meer over gaan leren. Ik heb haar geholpen met o.a kleuren, hovers, images plaatsen etc. beetje basic dingen.

Ben had ondertussen de design rationale getypt in een document en ik heb daarvan de design gemaakt in indesign omdat hij daar nog niet heel goed mee over weg kon.
 
