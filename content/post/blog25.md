---
title: 5e studio dag, wireframes maken
date: 2017-11-29
---

## Week 3 Woensdag - 29/11/2017

* Low fid prototype schetsen
* Wireframes

Maandag was ik begonnen met het schetsen van wireframes. Vandaag heb ik dat afgemaakt en is er een
design gekozen en hebben wij het concept vastgelegd.
