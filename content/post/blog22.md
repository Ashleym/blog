---
title: Let's begin!
date: 2017-11-20
---

## Week 2 Maandag - 20/11/2017

* Planning
* Mindmap Paard
* Debrief
* Merkanalyse

Ik heb vandaag een planning in Excel gemaakt zodat iedereen een duidelijk overzicht heeft van wat er per week af en ingeleverd moet worden. Daaruit is er een taakverdeling gedaan van deze week.
Deze week moeten de Debrief en merkanalyse ingeleverd worden.
Ik heb een begin gemaakt aan de merkanalyse.
