---
title: Dag 1 - Iteratie 1
date: 2017-09-04
---
Let's begin!

<!--more-->

### Week 1 Maandag - 04/09/2017

* Inventarisatie
* Team afspraken
* Gezamenlijke doelen opstellen
* Planning

Inventarisatie van het team op een poster gezet. Daarin staat onze achtergrond, kwaliteiten en ambities. Wij hebben teamafspraken en gezamenlijke doelen gesteld (staat op pdf).

Wij hebben per tweetal onderzoek gedaan naar een doelgroep van het HR en een thema uit Rotterdam. Daarvan hebben wij allen een concept (incl moodboard) gemaakt. Later in de middag hebben wij die met elkaar besproken en al wat ideeën met elkaar uitgewisseld.

Aan het einde van de dag heb ik een planning gemaakt en de taakverdeling in een document voor de komende twee weken.


![#Afspraken](../img/team_afspraken.JPG)
