---
title: Ordenen!  
date: 2017-12-11
---

## Week 5 Maandag - 11/12/2017

* Ordenen
* Verbeteren merkanalyse
* Nieuwe concepten

Ik heb de Drive en alles vandaag geordend zodat er weer even een duidelijk overzicht is. We zijn in Trello gaan kijken of alle taken van afgelopen weken wel allemaal afgevinkt zijn en wat er nog open staat. Er staat voor deze week ging deliverable op de planning.

Vorige week zijn we gaan brainstormen voor o.a nieuwe concepten daar zijn we vandaag mee verder gegaan en ik heb nog aan de merkanalyse gewerkt.
