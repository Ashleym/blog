---
title: De expo is al deze week!
date: 2018-1-15
---

## Week 8 maandag - 15/1/2018

* Voorbereiden voor de expo
* Concept poster
* User journey toekomstig - aanpassen voor recap
* Merkanalyses - aanpassen voor recap

Vandaag was Ben ziek. We zijn met de rest van het team in de ochtend gaan bespreken wat er nog allemaal gedaan zou moeten worden en wat er voorbereid moet worden voor de expo. Ik zou de conceptposter gaan maken zodat dat bij de pitch kan worden gebruikt. Die heb ik vandaag afgemaakt. Floris was nog bezig met de recap. Daarin was de merkanalyse niet echt leesbaar dus heb ik dat aangepast. Ninne had het design van de User Journey gemaakt. Kwa design paste het niet in het totaal. Dus die heb ik helemaal aangepast, ze had namelijk paar keer een pdf gestuurd in plaats van indd bestand. Het kostte veel tijd om alles in een nieuw document te zetten.
