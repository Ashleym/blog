---
title: Teamvorming en elkaar leren kennen
date: 2017-11-15
---

## Woensdag - 15/11/2017

* Teamvorming
* Verdeling doelgroepen
* Inventarisatie team

Vandaag werden de teams samengesteld. Ik had verwacht dat Elske en Bob de teamvorming zouden doen. Het bleek dat we die zelf samen mochten stellen wat fijn was. Het duurde wel langer dan verwacht omdat, je niet met iemand van je vorige team in het groepje mocht.

Het team waar ik nu in zit: Ninne, Rowie, Ben en Floris. Als doelgroep hebben we de "Nieuweling" gekregen.

Ik heb een inventarisatie van het team gedaan en dat in document gezet. Daarnaast heb ik meteen een map in Google Drive gemaakt en iedereen daarin toegevoegd zodat we volgende week dat niet meer hoeven te doen en aan de slag kunnen.

Voorderest was het vandaag een beetje elkaar beter leren kennen.
