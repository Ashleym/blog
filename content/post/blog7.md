---
title: Dag 7 - Iteratie 1
date: 2017-09-14
---

## Week 2 Donderdag - 14/09/2017

* Game analyse
* Foto’s maken
* Presentatie

Vandaag hebben Hannad en ik een game analyse afgenomen bij onze klasgenoot Aiden. Hij vond het design van de game duidelijk en overzichtelijk. Tijdens het spel was er wel een punt dat niet erg duidelijk vond: hij wist niet wat er bij een bepaalde pagina moest gebeuren en wanneer je foto van een angle goed is. Dat was inderdaad nog niet helemaal logisch. Als feedback kregen wij dat we misschien wat meer info erbij moeten geven. Ook kwamen wij erachter dat er een overbodige pagina was, die hebben wij nu weggelaten.

Wij hebben nog wat foto’s gemaakt van tijdens de analyse en nog wat foto’s voor in de presentatie. Daardoor heb ik het maken van de presentatie vandaag kunnen afronden.
