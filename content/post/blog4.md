---
title: Dag 4 - Iteratie 1
date: 2017-09-11
---

## Week 2 Maandag - 11/09/2017

* Concept aangepast
* Concept vastgelegd

Wij hebben vandaag een observatie gedaan, spelanalyse. Dat hebben wij eerst gedaan door het spel uit te leggen aan een student van het tweede jaar. Voor haar was het blijkbaar nog niet helemaal logisch, ze vond het doel van ons spel nog niet erg sterk. Vanuit haar feedback hebben wij wat aanpassingen gedaan voor onze game. Uiteindelijk zijn wij van doelgroep veranderd, informatica -> cmd, het doelgroep cmd past toch beter bij onze game omdat zij oplossingen voor een probleem verzinnen.  Daardoor hebben wij ook wat aanpassingen aan het concept gedaan. Uiteraard hebben wij wel nog het focus op de augmented reality gezet.

Concept #2
Je gaat in groepjes door Rotterdam lopen, de map laat zien waar de gebouwen zijn en hoe je moet lopen. Je gaat een puzzel oplossen van meerdere gebouwen bij elkaar. De gebouwen worden weergegeven in een zwartte silhouette. Zodra je bij het gebouw bent moet je van 5 (aantal van je groep) verschillende punten (angles) een foto nemen om het silhouetje zichtbaar te maken en vrij te spelen als AR miniatuur. Na het downloaden van de app word automatisch de kaart van Rotterdam gedownload zodat je het ook offline kan spelen. Ook kun je altijd offline de gebouwen bekijken die je al hebt vrij gespeeld. Het doel van het spel is zoveel mogelijk gebouwen te verzamelen.
