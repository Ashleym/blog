---
title: Dag 15 - Iteratie 3
date: 2017-10-09
---

## Week 6 Maandag - 09/10/2017

* Taak verdeling
* Paper prototype / 3D model maken
* Voorbereiding expo
* UI gemaakt

Vandaag is er een nieuwe taakverdeling gemaakt van wat er nog allemaal af moet. Er was te horen gekregen dat er een expo zou zijn op de woensdag na de vakantie. We hebben gekeken naar wat we allemaal nodig hebben voor de expo. Er naast de digitale prototype ook nog een paperprototype gemaakt. Omdat er een functie in een game is van: als je een gebouw unlocked je het gebouw in augmented reality ziet, is er een 3D gebouw gevouwen. Dat gebouw word dan op de kaart gezet (plattegrond word uitgeprint). Er is vandaag ook een User Interface gemaakt. De studenten/docenten die op de expo komen kijken kunnen dan zien hoe de app eruit ziet en werkt.
