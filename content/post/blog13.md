---
title: Dag 13 - Iteratie 2
date: 2017-10-02
---

## Week 5 Maandag - 02/10/2017

* Puntjes op de i gezet
* Icons gemaakt
* Drive ordenen
* Feedback

We hebben vandaag de puntjes op de i gezet, de controller helemaal uitgewerkt, presentatie afgemaakt en we hebben nog even met z’n alles goed doorgenomen. We hebben met z’n allen individueel icons gemaakt (icons van Rotterdam) voor in de prototype.  Vandaag heb ik de drive ook nog even geordend om een duidelijk overzicht te krijgen en dat we alles goed en makkelijk kunnen terug vinden. We hebben aan het eind van de dag feedback van bob gekregen. Het was erg positief en daardoor hebben wij nog ideeën gekregen die we als feature zouden kunnen doen. 
