---
title: Dag 8 - Iteratie 2
date: 2017-09-18
---

## Week 3 Maandag - 18/09/2017

* Presentatie
* Analyse bestaande spel

Vandaag hebben wij de presentatie gedaan en hebben wij die van onze klasgenoten bekeken. Daarna zijn wij per 2 tal begonnen aan de spelanalyse van 3 bestaande spellen. Jiska en ik hebben het spel Tetris gekozen. Wij hebben zelf het spel gedownload en gespeeld. Daarbij keken wij naar waarom het spel verslavend is, of de spelregels duidelijk zijn voor als je het spel nog niet kent etc.
