---
title: Dag 6 - Iteratie 1
date: 2017-09-13
---

## Week 2 Woensdag - 13/09/2017

* Presentatie paper prototype  
* Feedback
* Individuele opdracht aangepast
* Presentatie maken
* Workshop beeld analyseren

In de ochtend waren de presentaties van de paper prototypes. Als feedback kregen wij dat de paper prototype goed en duidelijk was. Als verbeterpunt kregen wij dat we het volgende keer beter met een fineliner konden overtrekken omdat het van verte nog al lastig te zien was, wat op de paper prototype stond. Wij hadden als voorbeeld gegeven dat als iedereen foto’s moet maken van een perspectief je dan in contact met elkaar komt doordat je dan moet communiceren wie van welke kant gaat maken. Maar als voorbeeld konden wij beter zeggen dat als iemand het iets fout doet dat je dan tegen diegene zegt van “kijk het moet zo”. Dat het eigenlijk dan juist de bedoelen is dat je fouten maakt en dat, dat helemaal niet erg is.

Doordat wij van doelgroep zijn veranderd moest ik mijn mood board voor de doelgroep veranderen. Daarnaast ben ik vandaag begonnen met het maken van de presentatie.

[#Workshop](#Workshop)
