---
title: Dag 16 - Iteratie 3
date: 2017-10-11
---

## Week 6 Woensdag - 11/10/2017

* Voorbereiding expo
* Filmpje maken
* Werken aan blog
* Workshop leerdossier

Vandaag zijn er nog een aantal wijzigingen gedaan aan het project, om het te verbeteren. Ik heb een filmpje gemaakt van hoe de leider bal een route aflegd om een gebouw vrij te spelen. Ik had het eerst in Flash gemaakt. Dat ging goed. Maar op een gegeven moment tripte dat een beetje. Ik heb het daarna in After Effects proberen te maken. Dat werkte ook niet zo. Uiteindelijk heb ik het in Photoshop en Illustrator gemaakt. Daarbij heb ik het per frame eerst in Illustrator gemaakt en dat ge-importeerd naar Photoshop als smart object en in Photoshop in een timeline gedaan. 
