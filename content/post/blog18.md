---
title: Dag 18 - Iteratie 3
date: 2017-10-25
---

## Week 8 Woensdag - Expo - 25/10/2017

* Expo
* Leerdossier/peerfeedback
* Study coaching

Vandaag was de expo. Het begon om 10.30 dus we hadden genoeg tijd om alles klaar te zetten. We hebben vandaag leuke feedback gekregen en ook feedback waaraan wij iets hadden. In totaal kregen wij 16 stickers (stemmen).

<!-- more -->

Na de  expo hadden we 2 uur tussen uur. In die tijd heb ik aan het leerdossier gewerkt en peerfeedback gekregen/gegeven.

Om 3 uur begon study coaching van Gerhard. Daar hebben wij nog een aantal dingen besproken over het leerdossier en peerfeedback. Daarbij kon iedereen hun vragen stellen.
