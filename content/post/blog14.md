---
title: Dag 14 - Iteratie 2
date: 2017-10-04
---

## Week 5 Maandag - 04/10/2017

* Presentatie
* Studie coaching

Vandaag waren de presentaties. Ik heb deelgenomen aan het presenteren, het stukje “onderzoek”. We kregen feedback van alumni’s. Het was niet erg positief. Het concept was blijkbaar niet zo duidelijk voor hun. Volgens hun moesten we wat meer focussen op het concept.

Eind van de middag kregen we weer studie coaching van Gerhard. Daar hebben we de STAR besproken, hoe dat te werk gaat. Daar heb ik ook meteen de STAR toegepast van wat er niet helemaal goed ging tijdens de presentatie etc.
