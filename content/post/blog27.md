---
title: Pitch!
date: 2017-12-6
---

## Week 4 Woensdag - 6/12/2017

* Pitch
* Verwerken feedback
* Brainstormen

Vandaag moest er gepitcht worden. We hebben het met z'n alle (behalve Ninne, die was er toen niet) in de ochtend de pitch voorbereid kwa visueel werk, wat we wilden laten zien zoals de schetsen en een voorbeeld van de concept om er een beter beeld erbij te krijgen. Ben heeft de Pitch gehouden.

De pitch ging goed, we hadden wat feedback gekregen. Na de pitch hebben wij dat verwerkt.

Er waren een aantal punten waarop er verbeterd kon worden. Het concept was nog niet sterk genoeg. Dat bleek na de feedback die we hadden gekregen. Daardoor zijn wij weer gaan brainstormen over hoe we het sterker kunnen maken.
