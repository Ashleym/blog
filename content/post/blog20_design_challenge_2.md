---
title: Design challenge 2 - Kick-off
date: 2017-11-14
---

## Week 1 Dinsdag 14/14/2017 - Kick-off Design challenge 2

Vandaag vond de Kick-off/intro van Design challenge 2 plaats in Den Haag, Paard.

Daar werd het nieuwe project geintroduceerd en kregen we een intro van Design challenge 2. Iemand van Paard gaf ons wat informatie over Paard. Daarnaast was er nog een spreker. Dat was een medewerker van Fabrique. Hij vertelde wat ons project was.

Het project is dat we een probleem moeten oplossen
van Paard; ze hebben bijna geen herhaal bezoek. Hoe kunnen wij
ervoor zorgen dat dat wel gaat gebeuren. We hebben een doelgroep
gekregen, namelijk de nieuweling. Wij gaan onderzoeken waarom
deze mensen niet terugkomen nadat ze een keer zijn geweest. Als we
antwoord hebben op deze vraag dan kunnen we gaan bedenken wat
we gaan ontwerpen zo dat zij terugkomen. Dit doen we in de vorm
van een klickable app. Het moet zowel in het voordeel van de
mensen, als in het voordeel van Paard zijn.
