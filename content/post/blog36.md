---
title: Expo!
date: 2018-1-17
---

## Week 8 woensdag - 17/1/2018

Het was de bedoeling dat er vandaag in de middag voorbereidingen zouden worden gedaan voor de expo. Doordat ik naar een begrafenis was heb ik mij pas later kunnen aansluiten. Van te voren had ik mijn conceptposter al in de Drive gezet zodat mijn teamgenoten die konden uitprinten.

Ben ging bij ons pitchen en Bob kwam bij ons feedback geven. We krege positieve feedback. Hij kwam met
extra ideeen voor het concept. Jammer dat wij niet eerder feedback van Bob hadden gevraagd.

De expo was erg leuk! En ik heb ook naar het werk van andere klasgenoten keken wat ook erg interessant
was. Doordat alle concepten erg verschillend waren maakte dat het extra leuk.
