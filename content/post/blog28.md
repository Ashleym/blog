---
title: Merkanalyse validatiemoment 1  
date: 2017-12-8
---

## Week 4 Vrijdag - 8/12/2017

* Validatie

Normaal ben ik vandaag vrij. Via mijn klasgenoten hoorde ik gister dat er vandaag een validatiemoment was voor de merkanalyse. Het bleek dat, dat al 2 weken zo was. Maar dat heb ik nooit geweten omdat het niet in de planning stond.

Ik ben naar school gegaan voor de validatie. Het ging per groepje. We gingen die van iedereen van het groepje bekijken en iedereen kon feedback geven. Later had Eelco feedback gegeven.

Er moest blijkbaar 3 verschillende merkanalyses gemaakt worden, dat ik niet wist omdat het nergens aangegeven stond. Daardoor moet ik volgende week terug komen. Eelco heeft mij voor de rest een voorbeeld meegegeven en aan de hand van de feedback weet ik nu hoe ik verder kan.
