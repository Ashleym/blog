---
title: Dag 3 - Iteratie 1
date: 2017-09-07
---

## Week 1 Donderdag - 07/09/2017

* Concept aangepast
* Vraagstelling
* Wireframes / design

Vandaag hebben wij gebrainstormd over hoe wij de paper prototype gaan maken en al wat designs gemaakt voor de user interface. Daarbij heb ik vooral gekeken bij wat het meest logische is voor bij voorbeeld het menu, waar wij die gaan plaatsen en het design van de icoontjes. Tijdens het maken van die schetsen zijn wij nog tegen wat problemen aangelopen over dat er sommige dingen toch niet zo logisch zijn zoals bijvoorbeeld dat het nog niet heel duidelijk is hoe het te werk gaat. Als oplossing hebben wij dan bijvoorbeeld een extra icoontje toegevoegd.
