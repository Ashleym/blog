---
title: 2e studio dag van het nieuwe project
date: 2017-11-22
---

## Week 2 Woensdag - 22/11/2017

* Trello
* Merkanalyse
* Workshop

Naast de planning in Excel heb ik ook een planning in Trello aangemaakt omdat, dat erg goed samen gaat en je krijgt dan een nog beter overzicht. Ik heb iedereen daarin toegevoegd en ook een aantal taken in gezet van de komende weken.

Tijdens de workshop uren heb ik een Workshop D-Scrum gevolgd van Bob. Ik heb al veel ervaring met Scrum door mijn stage. Toch was ik erg benieuwd wat D-Scrum inhield. Daar heb ik zeker weer nieuwe dingen geleerd.

Daarnaast ben ik vandaag verder gegaan met de merkanalyse.
