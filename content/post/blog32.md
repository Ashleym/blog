---
title: Laatste week, bijna vakantie!!  
date: 2017-12-18
---

## Week 6 Maandag - 18/12/2017

* Planning na lopen
* Wireframes afmaken
* Design rationale

Het is alweer de laatste week! Daarom zijn wij vandaag de planning gaan na lopen om te kijken of we op schema lopen.

Er moesten nog een aantal dingen worden gedaan omdat we een nieuw concept hebben/het hebben aangepast. Daardoor moest er bijvoorbeeld een nieuwe flowchart gemaakt worden etc. Alleen de wireframes voor het nieuwe concept en design moesten worden afgemaakt.

Daarnaast ben ik Ben gaan helpen met het Design rationale.
 
